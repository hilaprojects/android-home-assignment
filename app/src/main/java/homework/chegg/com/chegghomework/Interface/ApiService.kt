package homework.chegg.com.chegghomework.Interface

import homework.chegg.com.chegghomework.DataListA
import homework.chegg.com.chegghomework.networkDataFormat.DataC
import homework.chegg.com.chegghomework.networkDataFormat.DataListB
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Url

/**
 * REST API access points.
 * contains all the remote endpoints requests.
 */
interface ApiService {


    @GET()
    suspend fun getRemoteDataA(@Url url: String): Response<DataListA>

    @GET()
    suspend fun getRemoteDataB(@Url url: String): Response<DataListB>

    @GET()
    suspend fun getRemoteDataC(@Url url: String): Response<ArrayList<DataC>>
}