package homework.chegg.com.chegghomework.Interface

import homework.chegg.com.chegghomework.listener.INetworkListener
import homework.chegg.com.chegghomework.recyclerView.ViewHolderData

/**
 * DataSourceInterface holds all the data and functions the data sources should have.
 */
interface DataSourceInterface {

    val url: String
    var data: List<ViewHolderData>

    suspend fun fetchData(listener: INetworkListener)

    fun getDataSize(): Int {
        return data.size
    }

    fun getTimer(): Long

}