package homework.chegg.com.chegghomework

import android.app.Application
import android.content.Context
import homework.chegg.com.chegghomework.di.AppModule
import homework.chegg.com.chegghomework.di.ApplicationComponent
import homework.chegg.com.chegghomework.di.DaggerApplicationComponent


class MyApplication: Application() {
    // Reference to the application graph that is used across the whole app
    private lateinit var component: ApplicationComponent
    private lateinit var context: Context

    override fun onCreate() {
        super.onCreate()
        component = DaggerApplicationComponent.builder().appModule(AppModule()).build()
        context = applicationContext
    }

    fun getComponent(): ApplicationComponent {
        return component
    }

    fun getContext(): Context {
        return context
    }
}