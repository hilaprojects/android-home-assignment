package homework.chegg.com.chegghomework.activity

import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.ProgressBar
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import homework.chegg.com.chegghomework.viewModel.MainActivityViewModel
import homework.chegg.com.chegghomework.MyApplication
import homework.chegg.com.chegghomework.R
import homework.chegg.com.chegghomework.recyclerView.RecyclerViewAdapter
import kotlinx.coroutines.delay
import javax.inject.Inject

class MainActivity : AppCompatActivity() {
    private var toolbar: Toolbar? = null
    private var mRecyclerView: RecyclerView? = null
    private var mProgressBar: ProgressBar? = null
    private lateinit var mRecyclerViewAdapter: RecyclerViewAdapter
    @Inject
    lateinit var mViewModel: MainActivityViewModel
    override fun onCreate(savedInstanceState: Bundle?) {
        Log.d("MainActivity", "onCreate called")
        // Make Dagger instantiate @Inject fields in MainActivity
        (applicationContext as MyApplication).getComponent().inject(this)
        super.onCreate(savedInstanceState)
        buildUI()
    }

    override fun onStart() {
        super.onStart()
        mViewModel.initNetworkData()
    }

    override fun onStop() {
        super.onStop()
        mViewModel.stopNetworkDataReq()
    }

    override fun onResume() {
        super.onResume()
        mViewModel.restartNetworkReq()
    }

    private fun setupToolbar() {
        toolbar = findViewById<View>(R.id.toolbar) as Toolbar
        setSupportActionBar(toolbar)
    }

    private fun buildUI() {
        setContentView(R.layout.activity_main)
        setupToolbar()
        mRecyclerView = findViewById<View>(R.id.my_recycler_view) as RecyclerView
        mProgressBar = findViewById(R.id.progressBar)
        mRecyclerViewAdapter = RecyclerViewAdapter()
        mRecyclerView!!.adapter = mRecyclerViewAdapter
        mRecyclerView!!.layoutManager = LinearLayoutManager(this)
        mViewModel.mLiveDataObserverList.observe(this, Observer {dataList->
            if(dataList.isNotEmpty()) {
                mProgressBar!!.visibility = View.GONE
                mRecyclerViewAdapter.setData(dataList)
            } else {
                mRecyclerView!!.visibility = View.GONE
            }

        })

    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_main_activity, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_refresh -> {
                onRefreshData()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    /**
     * fetch data from all data sources, aggregate data and display in RecyclerView
     */
    private fun onRefreshData() {
        mViewModel.getNetworkData()
    }
}