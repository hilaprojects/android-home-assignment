package homework.chegg.com.chegghomework.di

import dagger.Module
import dagger.Provides
import homework.chegg.com.chegghomework.Interface.ApiService
import homework.chegg.com.chegghomework.managers.NetworkManager
import homework.chegg.com.chegghomework.networkDataSources.DataSourceA
import homework.chegg.com.chegghomework.networkDataSources.DataSourceB
import homework.chegg.com.chegghomework.networkDataSources.DataSourceC
import javax.inject.Singleton

@Module
class AppModule {
    @Singleton
    @Provides
    fun provideNetworkManager(): NetworkManager = NetworkManager()

    @Singleton
    @Provides
    fun provideDataSourceA(service: ApiService): DataSourceA = DataSourceA(service)

    @Singleton
    @Provides
    fun provideDataSourceB(service: ApiService): DataSourceB = DataSourceB(service)

    @Singleton
    @Provides
    fun provideDataSourceC(service: ApiService): DataSourceC = DataSourceC(service)

}