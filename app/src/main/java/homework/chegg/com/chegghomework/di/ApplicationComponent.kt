package homework.chegg.com.chegghomework.di

import dagger.Component
import homework.chegg.com.chegghomework.Interface.DataSourceInterface
import homework.chegg.com.chegghomework.activity.MainActivity
import javax.inject.Singleton

@Singleton
@Component(modules = [NetworkModule::class, AppModule::class])
interface ApplicationComponent {

    fun inject(activity: MainActivity)

    fun inject(dataSourceInterface: DataSourceInterface)
}