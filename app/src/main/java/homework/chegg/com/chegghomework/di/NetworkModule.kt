package homework.chegg.com.chegghomework.di

import dagger.Module
import dagger.Provides
import homework.chegg.com.chegghomework.Interface.ApiService
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
class NetworkModule {

    private val baseURL = "https://chegg-mobile-promotions.cheggcdn.com/android/homework/"

    @Provides
    fun provideApiService(retrofit:Retrofit): ApiService {
        return retrofit.create(ApiService::class.java)
    }

    @Singleton
    @Provides
    fun provideRetrofit(): Retrofit {
        return Retrofit.Builder()
                .baseUrl(baseURL)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
    }
}
