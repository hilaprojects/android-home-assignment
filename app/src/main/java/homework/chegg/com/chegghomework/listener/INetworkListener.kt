package homework.chegg.com.chegghomework.listener

interface INetworkListener {
    fun onError(msg: String)
}