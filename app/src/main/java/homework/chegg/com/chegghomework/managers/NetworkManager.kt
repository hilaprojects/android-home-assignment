package homework.chegg.com.chegghomework.managers

import android.util.Log
import androidx.lifecycle.MutableLiveData
import homework.chegg.com.chegghomework.Interface.DataSourceInterface
import homework.chegg.com.chegghomework.listener.INetworkListener
import homework.chegg.com.chegghomework.recyclerView.ViewHolderData
import kotlinx.coroutines.*
import java.util.*
import kotlin.concurrent.timerTask

/**
 * Network Manager manages all the network data sources:
 * fetching remote data from the network data sources (endpoints), aggregating the network data sources,
 * scheduling the network data sources (caching according to each timer)
 */
class NetworkManager {

    companion object{
        private const val TAG = "NetworkManager"
        private const val TO_MILI = 60 * 1000

    }

    var isNetworkAlarm = MutableLiveData<Boolean>()

    private val mNetworkLists: MutableList<NetworkSource> = mutableListOf()
    private val mNetworkData: MutableList<ViewHolderData> = mutableListOf()
    private var timer : Timer? = null

    /**
     * Add new network data source, init the schedule for each new data source according to
     * it's timer. The schedule will run permanently.
     * @param newSource is the new data source
     */
    fun addNetworkDataSource(newSource: DataSourceInterface) {
        val source = NetworkSource(newSource, false)
        mNetworkLists.add(source)
        if(timer == null)  { timer = Timer()}
        timer?.schedule(timerTask {
            Log.d(TAG, "Timer scheduled!")
            source.isTriggered = true
            if(isNetworkAlarm.value != true) {
                isNetworkAlarm.postValue(true)
            }
        }, newSource.getTimer() * TO_MILI, newSource.getTimer() * TO_MILI )
    }

    /**
     * Called when one of the endpoints timer terminates.
     * fetch data from the alarmed endpoints, aggregate the data from all data networks
     * and return the new aggregated list.
     * @param updateResult is a lambda func which receives the updated ViewHolderData list
     */
    fun runAlarm(updateResult: (result: MutableList<ViewHolderData>) -> Unit) {
        Log.d(TAG, "processAlarm")
        runBlocking {
            mNetworkLists.forEach { network ->
                if (network.isTriggered) {
                    launch {
                        fetchData(network)
                        network.isTriggered = false
                    }
                }
            }
        }
        arrangeNetworkData()
        updateResult(mNetworkData)
        isNetworkAlarm.postValue(false)
    }

    /**
     * Stop all the scheduled tasks
     */
    fun stopNetworkTimer() {
        timer?.cancel()
        timer = null
    }

    /**
     * Restart the schedule timer
     */
    fun restartNetworkTimer() {
        if(timer == null)  { timer = Timer()}
    }

    /**
     * Fetch data from all data sources, aggregate data and return it through lambda
     * @param updateResult is a lambda func which receives the updated ViewHolderData list
     */
    fun getNetworkData( updateResult: (MutableList<ViewHolderData>) -> Unit) {
        Log.d(TAG, "getNetworkData called")
        runBlocking {
            mNetworkLists.forEach { dataSource ->
                launch {
                    fetchData(dataSource)
                }
            }
        }
        arrangeNetworkData()
        updateResult(mNetworkData)
    }

    /**
     * Aggregate and rearrange the data sources.
     */
    private fun arrangeNetworkData() {
        Log.d(TAG, "arrangeNetworkData called")
        val networkSize = mNetworkLists.size
        var sumSize = 0
        var counter = 0
        var itemNum=0
        var dataIndex = 0
        mNetworkData.clear()
        //sum all the sources size
        mNetworkLists.forEach { item ->
            sumSize += item.networkSource.getDataSize()
        }

        while(counter < sumSize) {
            //check if all the items of the data source has been added
            if(mNetworkLists[itemNum].networkSource.getDataSize() > dataIndex) {
                Log.d(TAG, "adding: ${mNetworkLists[itemNum].networkSource.data[dataIndex]}")
                mNetworkData.add(mNetworkLists[itemNum].networkSource.data[dataIndex])
                counter++
            }

            itemNum++
            //restart the data sources iterations
            if(itemNum % networkSize == 0) {
                itemNum = 0
                dataIndex++
            }
        }
    }

    /**
     * Fetch network data source
     * @param dataSource
     */
    private suspend fun fetchData(dataSource: NetworkSource) {
        dataSource.networkSource.fetchData(object : INetworkListener {
            override fun onError(msg: String) {
                //In case the fetch fails, try again
                runBlocking {  launch { dataSource.networkSource.fetchData(object : INetworkListener {
                    override fun onError(msg: String) {
                        Log.e(TAG, msg)
                    }
                }) }}
            }
        })
    }

    /**
     * Private class which holds the data source and its triggered state respectively.
     */
    private data class NetworkSource(
        var networkSource: DataSourceInterface,
        var isTriggered: Boolean
    )
}