package homework.chegg.com.chegghomework


import com.google.gson.annotations.SerializedName

/**
 * DataSourceA json parsed data
 */

data class DataListA(
        @SerializedName("stories")val stories: List<DataA>

)

data class DataA(
        @SerializedName("title")val title: String,
        @SerializedName("subtitle")val subTitle: String,
        @SerializedName("imageUrl")val image: String
)
