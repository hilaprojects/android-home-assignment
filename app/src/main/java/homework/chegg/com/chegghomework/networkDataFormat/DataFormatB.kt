package homework.chegg.com.chegghomework.networkDataFormat

import com.google.gson.annotations.SerializedName

/**
 * DataSourceB json parsed data
 */
data class DataListB(
    @SerializedName("metadata")val metadata: InnerData

)

data class  InnerData(
        @SerializedName("innerdata")val innerData: List<DataB>
)

data class DataB(
    @SerializedName("picture")val picture: String,
    @SerializedName("articlewrapper")val articleWrapper: Headers,
)

data class Headers(
    @SerializedName("header")val header: String,
    @SerializedName("description")val description: String,
)

