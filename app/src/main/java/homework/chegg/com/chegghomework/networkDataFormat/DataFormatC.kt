package homework.chegg.com.chegghomework.networkDataFormat

import com.google.gson.annotations.SerializedName

/**
* DataSourceC json parsed data
*/
data class DataC(
        @SerializedName("topLine")val topLine: String,
        @SerializedName("subLine1")val subLine1: String,
        @SerializedName("subline2")val subline2: String,
        @SerializedName("image")val image: String,
)
