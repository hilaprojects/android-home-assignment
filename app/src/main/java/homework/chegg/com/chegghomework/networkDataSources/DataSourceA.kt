package homework.chegg.com.chegghomework.networkDataSources

import android.util.Log
import homework.chegg.com.chegghomework.*
import homework.chegg.com.chegghomework.Interface.ApiService
import homework.chegg.com.chegghomework.Interface.DataSourceInterface
import homework.chegg.com.chegghomework.listener.INetworkListener
import homework.chegg.com.chegghomework.recyclerView.ViewHolderData
import homework.chegg.com.chegghomework.utils.Consts.DATA_SOURCE_A_TIMER
import homework.chegg.com.chegghomework.utils.Consts.DATA_SOURCE_A_URL
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

/**
* DataSourceA endpoint. Responsible for fetching, parsing and storing data.
*/
class DataSourceA @Inject constructor(private val service: ApiService): DataSourceInterface {
        companion object {
                private const val TAG = "DataSourceA"
        }

        private val timer = DATA_SOURCE_A_TIMER
        override var url: String = DATA_SOURCE_A_URL
        override lateinit var data: List<ViewHolderData>

        init {
                data = mutableListOf()
        }

        override suspend fun fetchData(listener: INetworkListener) {
                Log.d(TAG, "fetchData called")
                withContext(Dispatchers.IO) {
                        val response = service.getRemoteDataA(url)
                        if (response.isSuccessful) {
                                data = parseDataToViewHolderData(response.body()!!)
                        } else {
                                listener.onError("getRemoteData failed")
                        }
                }
        }

        override fun getTimer(): Long {
                return timer
        }

        /**
         * Parsing the fetched data to ViewHolderData
         * @param data is fetched data
         */
        private fun parseDataToViewHolderData(data: DataListA): List<ViewHolderData> {
                Log.d(TAG, "parseDataToViewHolderData")
                val viewHolderDataList = mutableListOf<ViewHolderData>()
                data.stories.forEach{story->
                        viewHolderDataList.add(ViewHolderData(story.title, story.subTitle, story.image))
                }

                return viewHolderDataList
        }
}

