package homework.chegg.com.chegghomework.networkDataSources

import android.util.Log
import homework.chegg.com.chegghomework.Interface.ApiService
import homework.chegg.com.chegghomework.Interface.DataSourceInterface
import homework.chegg.com.chegghomework.listener.INetworkListener
import homework.chegg.com.chegghomework.recyclerView.ViewHolderData
import homework.chegg.com.chegghomework.networkDataFormat.DataListB
import homework.chegg.com.chegghomework.utils.Consts
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

/**
 * DataSourceB endpoint. Responsible for fetching, parsing and storing data.
 */
class DataSourceB @Inject constructor(private val service: ApiService): DataSourceInterface {

    companion object {
        private const val TAG = "DataSourceB"
    }

    private val timer = Consts.DATA_SOURCE_B_TIMER
    override var url: String = Consts.DATA_SOURCE_B_URL
    override lateinit var data: List<ViewHolderData>

    init {
        data = mutableListOf()
    }

    override suspend fun fetchData(listener: INetworkListener) {
        Log.d(TAG, "fetchData called")
        withContext(Dispatchers.IO) {
            val response = service.getRemoteDataB(url)
            if (response.isSuccessful) {
                data = parseDataToViewHolderData(response.body()!!)
            } else {
                listener.onError("getRemoteData failed")
            }
        }
    }

    override fun getTimer(): Long {
        return timer
    }

    /**
     * Parsing the fetched data to ViewHolderData
     * @param data is fetched data
     */
    private fun parseDataToViewHolderData(data: DataListB): List<ViewHolderData> {
        Log.d(TAG, "parseDataToViewHolderData")
        val viewHolderDataList = mutableListOf<ViewHolderData>()
        data.metadata.innerData.forEach{item->
            viewHolderDataList.add(ViewHolderData(item.articleWrapper.header, item.articleWrapper.description, item.picture))
        }

        return viewHolderDataList
    }
}
