package homework.chegg.com.chegghomework.networkDataSources

import android.util.Log
import homework.chegg.com.chegghomework.Interface.ApiService
import homework.chegg.com.chegghomework.Interface.DataSourceInterface
import homework.chegg.com.chegghomework.listener.INetworkListener
import homework.chegg.com.chegghomework.recyclerView.ViewHolderData
import homework.chegg.com.chegghomework.networkDataFormat.DataC
import homework.chegg.com.chegghomework.utils.Consts
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

/**
 * DataSourceC endpoint. Responsible for fetching, parsing and storing data.
 */
class DataSourceC @Inject constructor(private val service: ApiService): DataSourceInterface {

    companion object {
        private const val TAG = "DataSourceC"
    }

    private val timer = Consts.DATA_SOURCE_C_TIMER
    override var url: String = Consts.DATA_SOURCE_C_URL
    override lateinit var data: List<ViewHolderData>

    init {
        data = mutableListOf()
    }

    override suspend fun fetchData(listener: INetworkListener) {
        Log.d(TAG, "fetchData called")
        withContext(Dispatchers.IO) {
            val response = service.getRemoteDataC(url)
            if (response.isSuccessful) {
                data = parseDataToViewHolderData(response.body()!!)
            } else {
                listener.onError("getRemoteData failed")
            }
        }
    }

    override fun getTimer(): Long {
        return timer
    }

    /**
     * Parsing the fetched data to ViewHolderData
     * @param data is fetched data
     */
    private fun parseDataToViewHolderData(data: ArrayList<DataC>): List<ViewHolderData> {
        Log.d(TAG, "parseDataToViewHolderData")
        val viewHolderDataList = mutableListOf<ViewHolderData>()
        data.forEach{item->
            viewHolderDataList.add(ViewHolderData(item.topLine, item.subLine1 + item.subline2, item.image))
        }

        return viewHolderDataList
    }
}
