package homework.chegg.com.chegghomework.recyclerView

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import homework.chegg.com.chegghomework.R

class RecyclerViewAdapter: RecyclerView.Adapter<RecyclerViewAdapter.MyViewHolder>() {

    private var dataList = emptyList<ViewHolderData>()

    class MyViewHolder(view:View): RecyclerView.ViewHolder(view) {
        private val imageView: ImageView = view.findViewById(R.id.imageView_card_item)
        private val textItemTitle: TextView = view.findViewById(R.id.textView_card_item_title)
        private val textSubtitle: TextView = view.findViewById(R.id.textView_card_item_subtitle)
        fun bind(data: ViewHolderData) {
            textItemTitle.text = data.title
            textSubtitle.text = data.subTitle
            try {
                Glide.with(imageView)
                        .load(data.image)
                        .apply { RequestOptions.centerCropTransform() }
                        .error(R.drawable.no_image)
                        .into(imageView)
            } catch (e: Exception) {
                Log.e("RecyclerViewAdapter", "URL not found")
            }

        }


    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        return MyViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.card_item, parent, false))
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.bind(dataList[position])
    }

    override fun getItemCount(): Int {
         return dataList.size
    }

    fun setData(testResult: List<ViewHolderData>) {
        this.dataList = testResult
        notifyDataSetChanged()
    }

}