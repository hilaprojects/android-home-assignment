package homework.chegg.com.chegghomework.recyclerView

/**
 * ViewHolderData represents the data extracted from each data source.
 */
data class ViewHolderData (
    val title: String,
    val subTitle: String,
    val image: String
)