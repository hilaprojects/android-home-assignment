package homework.chegg.com.chegghomework.utils

object Consts {
    const val BASE_URL = "https://chegg-mobile-promotions.cheggcdn.com/android/homework/"
    const val DATA_SOURCE_A_URL = BASE_URL + "android_homework_datasourceA.json"
    const val DATA_SOURCE_B_URL = BASE_URL + "android_homework_datasourceB.json"
    const val DATA_SOURCE_C_URL = BASE_URL + "android_homework_datasourceC.json"

    const val DATA_SOURCE_A_TIMER = 5L
    const val DATA_SOURCE_B_TIMER = 30L
    const val DATA_SOURCE_C_TIMER = 60L
}