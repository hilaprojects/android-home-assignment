package homework.chegg.com.chegghomework.viewModel

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import homework.chegg.com.chegghomework.Interface.ApiService
import homework.chegg.com.chegghomework.networkDataSources.DataSourceA
import homework.chegg.com.chegghomework.managers.NetworkManager
import homework.chegg.com.chegghomework.recyclerView.ViewHolderData
import homework.chegg.com.chegghomework.networkDataSources.DataSourceB
import homework.chegg.com.chegghomework.networkDataSources.DataSourceC
import javax.inject.Inject

/**
* MainViewModel is the ViewModel that [MainActivity] uses to
* obtain information of what to show on the screen.
*/
class MainActivityViewModel @Inject constructor(private val mService: ApiService, private val mNetworkManager: NetworkManager): ViewModel() {

    companion object {
        const val TAG = "MainActivityViewModel"
    }

    //Notify the activity to show the fetched data on the RecyclerView
    var mLiveDataObserverList: MutableLiveData<List<ViewHolderData>> = MutableLiveData()

    init {
        mNetworkManager.isNetworkAlarm.observeForever{ isAlarm->
            Log.d(TAG, "isNetworkAlarm observer")
            if(isAlarm == true) {
                mNetworkManager.runAlarm{ data -> updateNetworkData(data) }
            }
        }
    }

    /**
     * init the network data sources and request data from the entered network sources.
     */
    fun initNetworkData() {
        Log.d(TAG, "initNetworkData called")
        //Add all the network data sources
        mNetworkManager.addNetworkDataSource(DataSourceA(mService))
        mNetworkManager.addNetworkDataSource(DataSourceB(mService))
        mNetworkManager.addNetworkDataSource(DataSourceC(mService))
        //Request for data
        getNetworkData()
    }

    /**
     * Request data from the network sources.
     */
    fun getNetworkData() {
        mNetworkManager.getNetworkData{ data -> updateNetworkData(data) }
    }

    /**
     * Stop network data Requests.
     */
    fun stopNetworkDataReq() {
        mNetworkManager.stopNetworkTimer()
    }

    /**
     * Restart network data Requests.
     */
    fun restartNetworkReq(){
        mNetworkManager.restartNetworkTimer()
    }

    /**
     * Notify the activity on new network data
     */
    private fun updateNetworkData(networkData: MutableList<ViewHolderData>) {
        mLiveDataObserverList.postValue(networkData)
    }
}